var isMenuOpen = false;
var openButton = $(".nav-open");
var closeButton = $(".nav-closed");
var menu = $(".menu");
var menuHeight = menu.height();
var menuBottom = menu.position().top + menuHeight;
var newHeight = window.height - menuBottom;
var menuList = $("nav>ul");

openButton.on("click", function (event){
    buttonClicked();
});

closeButton.on("click", function (event){
    buttonClicked();
});

function buttonClicked(){
    if (isMenuOpen){
        isMenuOpen = false;
        openButton.show();
        closeButton.hide();
        menuList.slideUp("slow");
    }else{
        isMenuOpen = true;
        openButton.hide();
        closeButton.show();
        menuList.slideDown("slow");
    }
}